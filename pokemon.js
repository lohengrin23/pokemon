const sectionSeleccionarAtaque = document.getElementById("seleccionar-ataque")
console.log('sectionSeleccionarAtaque', sectionSeleccionarAtaque);
const sectionReiniciar = document.getElementById("Reiniciar")
const botonMascotaJugador = document.getElementById("boton-mascota")
const botonFuego = document.getElementById("boton-Fuego")
const botonAgua = document.getElementById("boton-Agua")
const botonTierra = document.getElementById("boton-Tierra")
const botonReiniciar = document.getElementById("boton-reiniciar")

const inputHipodoge = document.getElementById("Hipodoge")
const inputCapipepo = document.getElementById("Capipepo")
const inputRatigueya = document.getElementById("Ratigueya")
const spanMascotaJugador = document.getElementById("Mascota-jugador")

const sectionSeleccionarMascota = document.getElementById("seleccionar-mascota");
const spanMascotaEnemigo = document.getElementById("Mascota-enemigo")

const SpanVidasJugador = document.getElementById("vidas-jugador")
const SpanVidasEnemigo = document.getElementById("vidas-enemigo")

const sectionMensajes = document.getElementById("resultado")
const ataquesDelJugador = document.getElementById("ataquesDelJugador")
const ataquesDelEnemigo = document.getElementById("ataquesDelEnemigo")


let Mokepones = []
let ataqueJugador
let ataqueEnemigo
let vidasJugador = 3
let vidasEnemigo = 3

class Mokepon {
    constructor(nombre, foto, vida) {
        this.nombre = nombre
        this.foto = foto
        this.vida = vida
        this.ataques = []
    }
}

let Hipodoge = new Mokepon("Hipodoge", "", 5)

let Capipepo = new Mokepon("Capipepo", "./assets/mokepons_mokepon_Capipepo_attack.png", 5)

let Ratigueya = new Mokepon("Ratigueya", "./assets/mokepons_mokepon_Ratigueya_attack.png", 5)

Hipodoge.ataques.push(
    { nombre: "💧", id: "boton-Agua" },
    { nombre: "💧", id: "boton-Agua" },
    { nombre: "💧", id: "boton-Agua" },
    { nombre: "🔥", id: "boton-Fuego" },
    { nombre: "🌳", id: "boton-Tierra" },
)

Capipepo.ataques.push(
    { nombre: "🌳", id: "boton-Tierra" },
    { nombre: "🌳", id: "boton-Tierra" },
    { nombre: "🌳", id: "boton-Tierra" },
    { nombre: "💧", id: "boton-Agua" },
    { nombre: "🔥", id: "boton-Fuego" },
)

Ratigueya.ataques.push(
    { nombre: "🔥", id: "boton-Fuego" },
    { nombre: "🔥", id: "boton-Fuego" },
    { nombre: "🔥", id: "boton-Fuego" },
    { nombre: "💧", id: "boton-Agua" },
    { nombre: "🌳", id: "boton-Tierra" },
)

//mokepones

Mokepones.push(Hipodoge,Capipepo,Ratigueya)

console.log(Mokepones)

console.log('quiero ver vidas enemigo', vidasEnemigo);

window.addEventListener("load", iniciarJuego)

window.addEventListener("load", (event) => {
    console.log("page is fully loaded", event);
    this.iniciarJuego;
    })

function iniciarJuego() {
    console.log('arrancò');   
    sectionSeleccionarAtaque.style.display = "none"   
    sectionReiniciar.style.display = "none"   
    botonMascotaJugador.addEventListener("click", seleccionarMascotaJugador )    
    botonFuego.addEventListener("click", ataqueFuego)  
    botonAgua.addEventListener("click", ataqueAgua)    
    botonTierra.addEventListener("click", ataqueTierra)
    console.log('botonTierra: ', botonTierra);   
    console.log('boton-reiniciar', botonReiniciar);
    botonReiniciar.addEventListener("click", ReiniciarJuego)   
}
     
function seleccionarMascotaJugador() {
    
    console.log('sectionSeleccionarMascota', sectionSeleccionarMascota);
    sectionSeleccionarMascota.style.display = "none"

    
    sectionSeleccionarAtaque.style.display = "flex" 

   
    if (inputHipodoge.checked) {
        spanMascotaJugador.innerHTML = "Hipodoge"
    } else if (inputCapipepo.checked) {
        spanMascotaJugador.innerHTML = "Capipepo"
    } else if (inputRatigueya.checked) {
        spanMascotaJugador.innerHTML = "Ratigueya"
    } else {
        alert("selecciona una mascota")
    }
}
    
    function seleccionarMascotaEnemigo() {
        let mascotaAleatoria = aleatorio(1,3)
    if (mascotaAleatoria == 1) {
        spanMascotaEnemigo.innerHTML = "Hipodoge"
    } else if (mascotaAleatoria == 2) {
        spanMascotaEnemigo.innerHTML = "Capipepo"
    } else {
        spanMascotaEnemigo.innerHTML = "Ratigueya"
    }
}

function ataqueFuego() {
    ataqueJugador = "FUEGO"
    ataqueAleatorioEnemigo()
}
function ataqueAgua() {
    console.log('agua');
    ataqueJugador = "AGUA"
    ataqueAleatorioEnemigo()
}
function ataqueTierra() {
    ataqueJugador = "TIERRA"
    ataqueAleatorioEnemigo()
}

function ataqueAleatorioEnemigo() {
    let ataqueAleatorio = aleatorio(1,3)

    if (ataqueAleatorio == 1) {
        ataqueEnemigo = "FUEGO"
    } else if (ataqueAleatorio == 2) {
        ataqueEnemigo = "AGUA"
    } else {
        ataqueEnemigo = "TIERRA"
    }

    combate()

}

function combate() {
   

    if(ataqueEnemigo == ataqueJugador) {
        crearMensaje("EMPATE")
    } else if(ataqueJugador == "FUEGO" && ataqueEnemigo == "TIERRA") {
        crearMensaje("GANASTE")
        vidasEnemigo--
        SpanVidasEnemigo.innerHTML = vidasEnemigo
    } else if(ataqueJugador == "AGUA" && ataqueEnemigo == "FUEGO") {
        crearMensaje("GANASTE")
        vidasEnemigo--
        SpanVidasEnemigo.innerHTML = vidasEnemigo
    } else if(ataqueJugador == "TIEERA" && ataqueEnemigo == "AGUA") {
        crearMensaje("GANASTE")
        vidasEnemigo--
        SpanVidasEnemigo.innerHTML = vidasEnemigo
    } else { 
        crearMensaje("PERDISTE")
        vidasJugador--
        SpanVidasJugador.innerHTML = vidasJugador
    }
    
    RevisarVidas()
}

function RevisarVidas() {
    if (vidasEnemigo == 0) {
        crearMensajeFinal=("FELICITACIONES GANASTE :)")
        let sectionReiniciar = document.getElementById("Reiniciar")
    sectionReiniciar.style.display = "block"
    } else if (vidasJugador == 0) {
        crearMensajeFinal=("lo siento, PERDISTE :(")
        let sectionReiniciar = document.getElementById("Reiniciar")
    sectionReiniciar.style.display = "block"
    } 
}

function crearMensaje(resultado) {


    let nuevoAtaqueDelJugador = document.createElement("p")
    let nuevoAtaqueDelEnemigo = document.createElement("p")

    sectionMensajes.innerHTML = resultado
    nuevoAtaqueDelJugador.innerHTML = ataqueJugador
    nuevoAtaqueDelEnemigo.innerHTML = ataqueEnemigo

    sectionMensajes.appendChild(nuevoAtaqueDelJugador)
    sectionMensajes.appendChild(nuevoAtaqueDelEnemigo)
}

function ReiniciarJuego() {
    console.log('reinicio');
    location.reload()
}

function crearMensajeFinal(resultadoFinal) {
    

    sectionMensajes.innerHTML = ResultadoFinal
    
    sectionMensajes.appendChild(parrafo)

    
    botonFuego.disable = true
    
    botonAgua.disable = true
    
    botonTierra.disable = true

    
    sectionSeleccionarAtaque.style.display = "none"

}

function aleatorio(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}
